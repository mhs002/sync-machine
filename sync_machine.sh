#!/bin/bash

# Array to store the list of devices
declare -a list_of_files 

cat << __EOF__
Welcome to the automatic sync machine!
Please enter the path of the file you want to sync
__EOF__

read file_name_entered 

# Check if the file name exists
while [[ ! -e $file_name_entered ]];do
       echo -ne "The file name you entered does not exist please enter another file: "	
       read file_name_entered
done 

# Add the list of devices if one device sync automatically, no device exit, many devices list them
for device in $(ls /media/*);do
	list_of_files+=("$device")
done 

if (( ${#list_of_files[@]} > 1 ));then

	echo ${list_of_files[@]}
	
	echo -ne "Please choose from the following devices: "
	read device_entered

elif (( ${#list_of_files[@]} == 0 ));then
	echo "No device present, enter a device and restart again."
	exit 1
else 
	device_entered=${list_of_files[0]}
	echo "There is only this device $device_entered so the system will automatically sync to it..."
fi 

# Checks if the device the user entered is present 
while (( 1 > 0 ));do
	test -e /media/*/$device_entered && break || echo -ne "Please choose a device from the ones you entered: "; read device_entered 
done 

echo -ne "Do you want to sync the whole file?[Y/n]"
read answer_entered

#In the line after || delete the extension and enter the extension of the file you want to sync
test $answer_entered == "Y" && $(rsync -r $file_name_entered/ /media/*/$device_entered) || $(rsync -r $file_name_entered/*.gz /media/*/$device_entered) 

